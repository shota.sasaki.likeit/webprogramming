package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;
/**
 * Servlet implementation class NewResister
 */
@WebServlet("/NewResister")
public class NewResisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewResisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userresister.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName=request.getParameter("userName");
		String birthDate=request.getParameter("birthDate");


		UserDao userDao = new UserDao();

		//戻り値取得、入力したloginIdで検索して結果表にそれ関係のデータが無ければnullが来る
		User user = userDao.findByLoginId(loginId);

		if(user!=null) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userresister.jsp");
		dispatcher.forward(request, response);
		}

		else if(!(password.equals(passwordConf))) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userresister.jsp");
			dispatcher.forward(request, response);
		}

		else if(loginId.isEmpty()||password.isEmpty()||passwordConf.isEmpty()||userName.isEmpty()||birthDate.isEmpty()) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userresister.jsp");
			dispatcher.forward(request, response);
		}

		userDao.insertuser(loginId,password,passwordConf,userName,birthDate);
		response.sendRedirect("UserListServlet");


		        }
	}


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="common.css">
</head>

    <body>

      <div class="text-center col-sm-9">
        <big>新規登録</big>
    </div>
    <br>
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="l navbar-nav flex-row">
        <li class="nav-item">
             <a class="nav-link" href="#">  ${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
            <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>
                </ul>
           </nav>
        <br>



    <form method="post" action="#" class="form-horizontal">
      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-5">
          <input name="loginId" type="text" class="form-control" id="loginId">
        </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-5">
          <input name="password" type="password" class="form-control" id="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
        <div class="col-sm-5">
          <input name="passwordConf" type="password" class="form-control" id="passwordConf">
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-5">
          <input name="userName"  type="text" class="form-control" id="userName">
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-5">
          <input name="birthDate" type="date" class="form-control" id="birthDate">
        </div>
      </div>

       <div class="submit-button-area">
         <button type="submit" value="検索" class="col-sm-2""btn btn-primary btn-lg btn-block">登録</button>
          </div>
        <br>
           <a class="btn btn-primary" href="UserListServlet">戻る</a>



        </form>
    </body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="common.css">

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
   <ul class="l navbar-nav flex-row">
         <li class="nav-item">
                <a class="nav-link" > ${userInfo.name}さん</a>
            </li>
         <li class="nav-item">
              　　  <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
            </li>
        </ul>
    </nav>

<c:forEach var="user" items="${userList}" >
    <div class="container">
        <div class="delete-area">
            <p>ログインID:${user.loginId}<br>を本当に削除してもよろしいですか？</p>
            <div class="row">
                <div class="col-sm-6">
                    <a href="" class="btn btn-light btn-block">キャンセル</a>
                </div>
                <div class="col-sm-6">
                    <form  action="UserDeleteServlet?id=${user.id}" method="post" >  <input class="btn btn-primary btn-block" type="submit" value="OK"></form>
                </div>
            </div>
        </div>
    </div>
    </c:forEach>
</body>

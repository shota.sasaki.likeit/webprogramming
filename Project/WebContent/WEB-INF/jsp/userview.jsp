<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!-- 上の二つのタグは絶対必須 -->
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="userview.css">
    <link rel="stylesheet" href="common.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="l navbar-nav flex-row">
            <li class="nav-item">
                <a class="nav-link" href="#"> ${userInfo.name}さん</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
            </li>
        </ul>
    </nav>
    <br>
    <div class="text-center">
        <font size="6">ユーザ一覧</font>
    </div>
    <br>
    <div class="text-right"><a href="NewResister" >新規登録</a></div>

     <form class="form-userlist" action="UserListServlet" method="post">

        <div class="form-group row">
            <label for="inputid" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-5">
                <input name="loginId" type="text" class="form-control" id="loginId"/>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputusername" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-5">
                <input name="userName"  type="text" class="form-control" id="userName"/>
            </div>
        </div>

        <label for="continent" class="control-label col-sm-2">生年月日</label>
        <div class="row col-sm-10">
            <div class="col-sm-5">
                <input type="date" name="date-start" id="date-start" class="form-control" size="30"/>
            </div>
            <div class="col-sm-1"> ~</div>
            <div class="col-sm-5">
                 <input type="date" name="date-end" id="date-end" class="form-control"/>
            </div>
        </div>
        <br>

        <input name=serch type=submit class="col-sm-2" "btn btn-light" value="検索">
    </form>
    <br>
    <p class="box"> </p>


    <form>

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">ログインID</th>
                    <th scope="col">ユーザ名</th>
                    <th scope="col">生年月日</th>
                    <th scope="col"></th>
                </tr>
            </thead>
   <tbody>

         <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細 </a>
              <c:if test="${userInfo.loginId=='admin'or userInfo.loginId==user.loginId}" >
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                   </c:if>
                <c:if test="${userInfo.loginId=='admin'}">
                         <a class="btn btn-danger"  href ="UserDeleteServlet?id=${user.id}">削除</a>
                         </c:if>

                     </td>



                   </tr>
                 </c:forEach>
               </tbody>
        </table>
    </form>
</body>

</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="common.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul  class="l navbar-nav flex-row">
            <li class="nav-item">
                <a class="nav-link" href="#">  ${userInfo.name}さん</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
            </li>
        </ul>
    </nav>

 <form class="form-update" action="UserUpdateServlet" method="post">


    <div class="text-center">
        <font size="6">ユーザー情報更新</font>
    </div>
     <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    <br>

  <c:forEach var="user" items="${userList}" >

    <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-5">
            <input name="loginId" class="form-control-plaintext" id="loginId" readonly="readonly" value=${user.loginId}>
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-5">
            <input type="password" class="form-control" id="password" name="password">
        </div>
    </div>

    <div class="form-group row">
        <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
        <div class="col-sm-5">
            <input type="password" class="form-control" id="passwordConf"    name="passwordConf">
        </div>
    </div>

    <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-5">
            <input type="text" class="form-control" id="userName" name="userName" value=${user.name}>
        </div>
    </div>

    <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-5">
            <input type="date" class="form-control" id="birthDate" value=${user.birthDate} name="birthDate">
        </div>
    </div>

    <div class="submit-button-area">
        <button type="submit" value="更新" class="col-sm-2""btn btn-primary btn-lg btn-block">更新</button>
    </div>
    <br>

      </c:forEach>
      </form>


       <a class="btn btn-primary" href="UserListServlet">戻る</a>



</body>

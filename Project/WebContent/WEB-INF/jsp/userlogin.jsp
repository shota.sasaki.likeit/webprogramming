<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="common.css">

</head>

<body>


    <div class="text-center col-sm-9">
        <big>ログイン画面</big>
    </div>
    <br>



      <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    <form class="form-signin" action="LoginServlet" method="post">

        <div class="form-group row">
            <label for="inputid" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-5">
                <input name="loginId" type="id" class="form-control" id="inputid">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
            <div class="col-sm-5">
                <input name="password" type="password" class="form-control" id="inputPassword">
            </div>
        </div>
        <br>

    <button type="submit" class="col-sm-2" "btn btn-light">ログイン</button>
    </form>
</div>

</body>

</html>

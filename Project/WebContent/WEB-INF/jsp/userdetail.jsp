<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="common.css">


<title>Insert title here</title>
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="l navbar-nav flex-row">

            <li class="nav-item">
                <a class="nav-link" href="#"> ${userInfo.name}さん</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary" href="LogoutServlet"">ログアウト</a>
            </li>
        </ul>
    </nav>
    <br>

     <c:forEach var="user" items="${userList}" >

    <div class="text-center">
        <font size="6">ユーザー情報詳細参照</font>
    </div>
    <br>
    <br>
    <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.loginId}</p>
        </div>
    </div>

    <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.name}</p>
        </div>
    </div>
    <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.birthDate}</p>
        </div>
    </div>

    <div class="form-group row">
        <label for="createDate" class="col-sm-2 col-form-label">登録日時</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.createDate}</p>
        </div>
    </div>

    <div class="form-group row">
        <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.updateDate}</p>
        </div>
    </div>
    <br>

  </c:forEach>

     <a class="btn btn-primary" href="UserListServlet">戻る</a>

</body>
</html>